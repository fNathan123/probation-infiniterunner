using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InfiniteRunner;
using InfiniteRunner.Object;

namespace InfiniteRunner.Score
{
	public class ScoreManager : MonoBehaviour
	{
		public static ScoreManager Instance { get; private set; }
		public int Score { get; private set; }

		private float distanceTraveled;

		private void Awake()
		{
			Instance = this;
			GameObject spawner = GameObject.Find("Spawner");
			spawner.GetComponent<Spawner>().AddScore += AddScore;

			GameObject runner = GameObject.Find("Runner");
			runner.GetComponent<Runner>().AddTraveledScore += AddScore;
		}

		void Start()
		{
			Score = 0;
		}

		public void AddScore(int value)
		{
			Debug.Log("Add score" + value);
			Score += value;
			//if (score < 0) score = 0;
		}
	}
}
