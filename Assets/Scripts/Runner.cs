using InfiniteRunner.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteRunner
{
	public class Runner : MonoBehaviour
	{
		public event Action<int> HealthChange;
		public event Action<int> AddTraveledScore;
		public event Action RunnerDead;
		//components
		private Animator anim;

		//runner status
		private RunnerState state;
		private Vector3 destination;
		private Vector3 initPos;
		private float turningSpeed;
		private float speed;
		private int health;

		private void Awake()
		{
			
		}
		void Start()
		{
			//init player stats
			turningSpeed = 15f;
			speed = 10f;
			health = 3;
			//UIManager.Instance.UpdateUIHealth(health);
			initPos = gameObject.transform.position;

			anim = GetComponent<Animator>();
		}

		void Update()
		{
			transform.position += new Vector3(1, 0, 0) * speed * Time.deltaTime;

			if (Input.GetKeyDown(KeyCode.A) && state == RunnerState.IDLE && GameplayManager.Instance.State != GameState.GAMEOVER)
			{
				MoveCommand("L");
			}
			if (Input.GetKeyDown(KeyCode.D) && state == RunnerState.IDLE && GameplayManager.Instance.State != GameState.GAMEOVER)
			{
				MoveCommand("R");
			}

			if (state == RunnerState.MOVING)
			{
				if (Mathf.Abs(destination.z - transform.position.z) < 0.01)
				{
					state = RunnerState.IDLE;
				}

				float dir = 1;
				if (destination.z < transform.position.z)
				{
					dir = -1;
				}

				transform.position += new Vector3(0, 0, 1) * dir * turningSpeed * Time.deltaTime;
			}
		}

		void MoveCommand(string direction)
		{
			Vector3 pos = transform.position;
			if (direction == "L")
			{
				if (pos.z < 10)
				{
					pos.z += 10;
				}
			}
			else if (direction == "R")
			{
				if (pos.z > -10)
				{
					pos.z -= 10;
				}
			}
			destination = pos;
			state = RunnerState.MOVING;
		}

		private void OnCollisionEnter(Collision collision)
		{
			ICollectable obj = collision.gameObject.GetComponent<ICollectable>();
			IObstacle obstacle = collision.gameObject.GetComponent<IObstacle>();
			if (obj!= null && obj.Type == ObjectType.COIN)
			{
				obj.CollideAction();
			}
			else if (obstacle != null && obstacle.Type == ObjectType.OBSTACLE)
			{
				//ScoreManager.Instance.AddScore(-100);
				//health--;
				//UIManager.Instance.UpdateUIHealth(health);
				//if (health > 0)
				//{
				//	Destroy(collision.gameObject);
				//}
				//else
				//{
				//	state = RunnerState.DEAD;
				//	speed = 0;
				//	int distTraveled = (int)Mathf.Floor(transform.position.x - initPos.x);
				//	Debug.Log(distTraveled + " " + ScoreManager.Instance.Score);
				//	ScoreManager.Instance.AddScore(distTraveled);
				//	anim.SetInteger("DeathType_int", 1);
				//	anim.SetBool("Death_b", true);
				//	//gameover
				//	GameplayManager.Instance.GameOver();
				//}
				obstacle.CollideAction();
				int damage = obstacle.Damage;
				health-=damage;
				HealthChange?.Invoke(health);
				if(health == 0)
				{
					state = RunnerState.DEAD;
					speed = 0;
					int distTraveled = (int)Mathf.Floor(transform.position.x - initPos.x);
					AddTraveledScore?.Invoke(distTraveled);
					anim.SetInteger("DeathType_int", 1);
					anim.SetBool("Death_b", true);
					RunnerDead?.Invoke();
				}
			}
		}
	}
}
