using InfiniteRunner;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace InfiniteRunner.UI
{
	public class UIManager : MonoBehaviour
	{
		public static UIManager Instance { get; private set; }

		[SerializeField]
		private GameObject gameplayUI;
		[SerializeField]
		private GameObject gameOverUI;

		[SerializeField]
		private Text scoreValueText;
		[SerializeField]
		private Text healthValueText;

		[SerializeField]
		private Button restartButton;

		private void Awake()
		{
			Instance = this;
			GameObject runner = GameObject.Find("Runner");
			runner.GetComponent<Runner>().HealthChange += UpdateUIHealth;

			restartButton.onClick.AddListener(RestartOnclick);
		}
		#region UI Update

		public void UpdateUIHealth(int value)
		{
			healthValueText.text = value.ToString();
		}

		public void UpdateUIScore(int value)
		{
			scoreValueText.text = value.ToString();
		}

		public void SetActiveGameplayUI(bool value)
		{
			gameplayUI.SetActive(value);
		}

		public void SetActiveGameOverUI(bool value)
		{
			gameOverUI.SetActive(value);
		}

		#endregion

		#region ButtonClick
		//For Button Triggers
		public void RestartOnclick()
		{
			GameplayManager.Instance.Restart();
		}

		#endregion
	}
}
