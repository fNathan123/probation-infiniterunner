using InfiniteRunner;
using InfiniteRunner.Score;
using InfiniteRunner.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InfiniteRunner
{
	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager Instance { get; private set; }
		public GameState State { get; private set; }


		void Awake()
		{
			Instance = this;
			State = GameState.PLAYING;
			GameObject runner = GameObject.Find("Runner");
			runner.GetComponent<Runner>().RunnerDead += GameOver;
		}

		public void GameOver()
		{
			State = GameState.GAMEOVER;
			int score = ScoreManager.Instance.Score;
			UIManager.Instance.UpdateUIScore(score);
			UIManager.Instance.SetActiveGameplayUI(false);
			UIManager.Instance.SetActiveGameOverUI(true);
		}

		public void Restart()
		{
			SceneManager.LoadScene("TempleRun");
		}
	}
}
