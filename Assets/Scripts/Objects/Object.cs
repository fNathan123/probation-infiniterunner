using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteRunner.Object
{
    public class Object : MonoBehaviour
    {
        private GameObject runner;

        void Start()
        {
            runner = GameObject.Find("Runner");
        }

        void Update()
        {
            //transform.position += new Vector3(1, 0, 0) * -1 * speed * Time.deltaTime;
            if(transform.position.x < runner.transform.position.x-10)
		    {
                Destroy(gameObject);
		    }
        }
    }
}
