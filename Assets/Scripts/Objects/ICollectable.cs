public interface ICollectable
{
    public ObjectType Type
	{
		get;
	}

	public void CollideAction();

}
