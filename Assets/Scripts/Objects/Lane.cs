using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteRunner.Object
{
    public class Lane : MonoBehaviour
    {
        GameObject runner;
        Vector3 initPos;
        void Start()
        {
            runner = GameObject.Find("Runner");
            initPos = transform.position;
        }

        void Update()
        {
            if(runner.transform.position.x > transform.position.x + 30)
		    {
                transform.position = initPos + new Vector3(100, 0, 0);
                initPos = transform.position;
		    }   
        }
    }
}
