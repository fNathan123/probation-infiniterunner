public interface IObstacle
{
	public ObjectType Type
	{
		get;
	}
	public int Damage
	{
		get;
	}

	public void CollideAction();
}
