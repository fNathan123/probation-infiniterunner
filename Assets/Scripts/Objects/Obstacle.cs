using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteRunner.Object
{
	public class Obstacle : MonoBehaviour,IObstacle
	{
		public Action<int> AddScore;
	
		private ObjectType _type;
		public ObjectType Type
		{
			get => _type;
			private set => _type = value;
		}

		private int _damage;
		public int Damage
		{
			get => _damage;
			private set => _damage = value;
		}

		private void Awake()
		{
			Type = ObjectType.OBSTACLE;
			Damage = 1;
		}

		public void CollideAction()
		{
			Destroy(gameObject);
			AddScore?.Invoke(-100);
		}
	}

}
