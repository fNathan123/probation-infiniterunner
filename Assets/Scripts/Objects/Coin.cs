using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteRunner.Object
{
	public class Coin : MonoBehaviour,ICollectable
	{
		public Action<int> AddScore;
		private ObjectType _type;

		public ObjectType Type 
		{ 
			get => _type; 
			private set => _type = value;
		}

		private void Awake()
		{
			Type = ObjectType.COIN;
		}

		public void CollideAction()
		{
			Destroy(gameObject);
			AddScore?.Invoke(50);
		}
	}
}
