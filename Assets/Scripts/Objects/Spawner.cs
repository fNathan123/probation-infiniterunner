using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteRunner.Object
{
    public class Spawner : MonoBehaviour
    {
        public event Action<int> AddScore;

        [SerializeField]
        GameObject coinPrefab;
        [SerializeField]
        GameObject obstaclePrefab;

        //spawner stats
        float speed;

	    private void Start()
	    {
            speed = 10f;
            InvokeRepeating("SpawnMovingObjects", 0.5f, 1.5f);
        }

        void SpawnMovingObjects()
	    {
            int rng = UnityEngine.Random.Range(0, 2);
            Vector3 pos = transform.position;

            int lane = UnityEngine.Random.Range(1, 4);
            if(lane == 1)
		    {
                pos.z = 10;
		    }
            else if(lane == 2)
		    {
                pos.z = 0;
		    }
		    else
		    {
                pos.z = -10;
		    }

            if (rng == 0)
		    {
                SpawnCoin(pos);
		    }
		    else
		    {
                SpawnObstacle(pos);
		    }
	    }

        void SpawnCoin(Vector3 pos)
	    {
            pos.y = 2.11f;
            Coin c = Instantiate(coinPrefab, pos, coinPrefab.transform.rotation).GetComponent<Coin>();
            c.AddScore = AddScore;
	    }

        void SpawnObstacle(Vector3 pos)
	    {
            Obstacle o = Instantiate(obstaclePrefab, pos, obstaclePrefab.transform.rotation).GetComponent<Obstacle>();
            o.AddScore = AddScore;
        }

	    void Update()
	    {
            transform.position += new Vector3(1, 0, 0) * speed * Time.deltaTime;
            if(GameplayManager.Instance.State == GameState.GAMEOVER)
		    {
                CancelInvoke();
		    }
	    }
    }

}
